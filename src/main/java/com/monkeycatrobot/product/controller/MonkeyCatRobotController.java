package com.monkeycatrobot.product.controller;

import javax.validation.Valid;

import java.util.List;

import com.monkeycatrobot.product.model.MonkeyCatRobot;
import com.monkeycatrobot.product.service.MonkeyCatRobotService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/monkeycatrobots")
public class MonkeyCatRobotController {
    private final MonkeyCatRobotService mCRobotService;
    private final Environment env;
    private int port;

    @Value("${hiyaluna}")
    private String hiyaluna;

    /*
     * If configuring a random port, this will retrieve port number
     * @EventListener
     * public void onApplicationEvent(final ServletWebServerInitializedEvent event) {
     *   port = event.getWebServer().getPort();
     *   System.out.println("The random port is: " + port);
     * }
     */

    @GetMapping("/status")
    public String checkStatus() {
        //        String message = hiyaluna + " MonkeyCatRobot Product Microservice is up on port: " + port;
        String message =
                hiyaluna + " MonkeyCatRobot Product Microservice is up on port: " + env.getProperty("server.port");
        return message;
    }

    @GetMapping("/count")
    public long getCount() {
        return mCRobotService.getCount();
    }

    @GetMapping("/all")
    public List<MonkeyCatRobot> getAll() {
        return mCRobotService.getAll();
    }

    @GetMapping("/id/{id}")
    public MonkeyCatRobot getById(@PathVariable Long id) {
        return mCRobotService.getById(id);
    }

    @GetMapping("/name/{name}")
    public List<MonkeyCatRobot> getByName(@PathVariable String name) {
        return mCRobotService.getByName(name);
    }

    @GetMapping("/customerid/{customerId}")
    public List<MonkeyCatRobot> getByCustomerId(@PathVariable String customerId) {
        return mCRobotService.getByCustomerId(customerId);
    }

    @GetMapping("/customerid/{customerId}/name/{name}")
    public List<MonkeyCatRobot> getByCustomerIdAndName(@PathVariable String customerId, @PathVariable String name) {
        return mCRobotService.getByCustomerIdAndName(customerId, name);
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createMonkeyCatRobot(@Valid @RequestBody MonkeyCatRobot monkeyCatRobot) {
        return mCRobotService.createMonkeyCatRobot(monkeyCatRobot);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<MonkeyCatRobot> updateMonkeyCatRobot(@PathVariable Long id, @Valid @RequestBody
            MonkeyCatRobot detailsMCR) {
        return mCRobotService.updateMonkeyCatRobot(id, detailsMCR);
    }

    @DeleteMapping("/recycle/{id}")
    public String deleteMonkeyCatRobot(@PathVariable Long id) {
        return mCRobotService.deleteMonkeyCatRobot(id);
    }
}
