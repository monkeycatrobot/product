package com.monkeycatrobot.product.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MONKEY_CAT_ROBOT")
public class MonkeyCatRobot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "customer_id", nullable = false)
    private String customerId;

    @Column(name = "head_type", nullable = false)
    private String headType;

    @Column(name = "head_color", nullable = false)
    private String headColor;

    @Column(name = "head_shape", nullable = false)
    private String headShape;

    @Column(name = "torso_type", nullable = false)
    private String torsoType;

    @Column(name = "torso_color", nullable = false)
    private String torsoColor;

    @Column(name = "torso_shape", nullable = false)
    private String torsoShape;

    @Column(name = "legs_type", nullable = false)
    private String legsType;

    @Column(name = "legs_color", nullable = false)
    private String legsColor;
}
