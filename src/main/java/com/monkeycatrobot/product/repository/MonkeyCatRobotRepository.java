package com.monkeycatrobot.product.repository;

import java.util.List;

import com.monkeycatrobot.product.model.MonkeyCatRobot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonkeyCatRobotRepository extends JpaRepository<MonkeyCatRobot, Long> {
    List<MonkeyCatRobot> findByNameIgnoreCase(String name);
    List<MonkeyCatRobot> findByCustomerId(String customerId);
    List<MonkeyCatRobot> findByCustomerIdAndNameIgnoreCase(String customerId, String name);
}
