package com.monkeycatrobot.product.service;

import java.util.List;

import com.monkeycatrobot.product.model.MonkeyCatRobot;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface MonkeyCatRobotService {
    public long getCount();
    public List<MonkeyCatRobot> getAll();
    public MonkeyCatRobot getById(Long id);
    public List<MonkeyCatRobot> getByName(String name);
    public List<MonkeyCatRobot> getByCustomerId(String customerId);
    public List<MonkeyCatRobot> getByCustomerIdAndName(String customerId, String name);
    public ResponseEntity<Object> createMonkeyCatRobot(MonkeyCatRobot monkeyCatRobot);
    public ResponseEntity<MonkeyCatRobot> updateMonkeyCatRobot(Long id, MonkeyCatRobot detailsMCR);
    public String deleteMonkeyCatRobot(Long id);
}
