package com.monkeycatrobot.product.service;

import java.util.List;

import com.monkeycatrobot.product.model.MonkeyCatRobot;
import com.monkeycatrobot.product.repository.MonkeyCatRobotRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class MonkeyCatRobotServiceImpl implements MonkeyCatRobotService {

    private final MonkeyCatRobotRepository repository;

    @Override
    public long getCount() {
        return repository.count();
    }

    @Override
    public List<MonkeyCatRobot> getAll() {
        List<MonkeyCatRobot> mCRs = repository.findAll();
        if(mCRs.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return mCRs;
    }

    @Override
    public MonkeyCatRobot getById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public List<MonkeyCatRobot> getByName(String name) {
        List<MonkeyCatRobot> monkeyCatRobot = repository.findByNameIgnoreCase(name);
        if(monkeyCatRobot.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return monkeyCatRobot;
    }

    @Override
    public List<MonkeyCatRobot> getByCustomerId(String customerId) {
        List<MonkeyCatRobot> monkeyCatRobot = repository.findByCustomerId(customerId);
        if(monkeyCatRobot.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return monkeyCatRobot;
    }

    @Override
    public List<MonkeyCatRobot> getByCustomerIdAndName(String customerId, String name) {
        List<MonkeyCatRobot> monkeyCatRobot = repository.findByCustomerIdAndNameIgnoreCase(customerId, name);
        if(monkeyCatRobot.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return monkeyCatRobot;
    }

    @Override
    public ResponseEntity<Object> createMonkeyCatRobot(MonkeyCatRobot monkeyCatRobot) {
        MonkeyCatRobot newMonkeyCatRobot;
        try {
            newMonkeyCatRobot = repository.save(monkeyCatRobot);
        }
        catch(Exception ex) {
            String message = ex.getCause().getCause().getMessage();
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(newMonkeyCatRobot, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<MonkeyCatRobot> updateMonkeyCatRobot(Long id, MonkeyCatRobot detailsMCR) {
        MonkeyCatRobot currentMCR =
                repository.findById(id)
                        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        currentMCR.setName(detailsMCR.getName());
        currentMCR.setCustomerId(detailsMCR.getCustomerId());
        currentMCR.setHeadType(detailsMCR.getHeadType());
        currentMCR.setHeadColor(detailsMCR.getHeadColor());
        currentMCR.setHeadShape(detailsMCR.getHeadShape());
        currentMCR.setTorsoType(detailsMCR.getTorsoType());
        currentMCR.setTorsoColor(detailsMCR.getTorsoColor());
        currentMCR.setTorsoShape(detailsMCR.getTorsoShape());
        currentMCR.setLegsType(detailsMCR.getLegsType());
        currentMCR.setLegsColor(detailsMCR.getLegsColor());

        final MonkeyCatRobot updatedMonkeyCatRobot = repository.save(currentMCR);

        return ResponseEntity.ok(updatedMonkeyCatRobot);
    }

    @Override
    public String deleteMonkeyCatRobot(Long id) {
        MonkeyCatRobot currentMCR = repository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        String name = currentMCR.getName();

        repository.delete(currentMCR);

        return String.format("MonkeyCatRobot with ID %s has been recycled. %s will be missed but not forgotten.", id,
                name);
    }
}
