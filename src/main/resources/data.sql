INSERT INTO MONKEY_CAT_ROBOT (name, customer_id, head_type, head_color, head_shape, torso_type, torso_color, torso_shape, legs_type, legs_color)
VALUES  ('Marvin', '12345-00', 'cat', 'orange', 'triangle', 'robot', 'purple', 'square', 'monkey', 'red');
INSERT INTO MONKEY_CAT_ROBOT (name, customer_id, head_type, head_color, head_shape, torso_type, torso_color, torso_shape, legs_type, legs_color)
VALUES  ('Robby', '12345-01', 'cat', 'blue', 'circle', 'monkey', 'red', 'triangle', 'robot', 'green');
INSERT INTO MONKEY_CAT_ROBOT (name, customer_id, head_type, head_color, head_shape, torso_type, torso_color, torso_shape, legs_type, legs_color)
VALUES  ('Rosie', '12345-02', 'robot', 'pink', 'square', 'cat', 'red', 'circle', 'monkey', 'blue');
INSERT INTO MONKEY_CAT_ROBOT (name, customer_id, head_type, head_color, head_shape, torso_type, torso_color, torso_shape, legs_type, legs_color)
VALUES  ('Robot B-09', '12345-03', 'monkey', 'green', 'circle', 'robot', 'yellow', 'square', 'cat', 'blue');
INSERT INTO MONKEY_CAT_ROBOT (name, customer_id, head_type, head_color, head_shape, torso_type, torso_color, torso_shape, legs_type, legs_color)
VALUES  ('T-1000', '12345-04', 'robot', 'red', 'triangle', 'cat', 'blue', 'square', 'robot', 'green');
INSERT INTO MONKEY_CAT_ROBOT (name, customer_id, head_type, head_color, head_shape, torso_type, torso_color, torso_shape, legs_type, legs_color)
VALUES  ('Unit3021', '12345-02', 'robot', 'pink', 'trapezium', 'monkey', 'blue', 'rectangle', 'cat', 'grey');
