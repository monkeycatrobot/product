package com.monkeycatrobot.product.controller;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monkeycatrobot.product.model.MonkeyCatRobot;
import com.monkeycatrobot.product.service.MonkeyCatRobotService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.*;

import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*
 * Almost all controller tests will be integration testing (https://reflectoring.io/spring-boot-web-controller-test/)
 * Unit testing will cover: verifying we called the mock service with expected arguments
 * Unit testing will not cover: listening for HTTP requests, deserializing input,
 * validating input, serializing output, translating exceptions
 * We need Spring context to do so
 * @WebMvcTest creates context, but only for beans needed to test the controller
 */
/*
 * @WebMvcTest does not start a server
 * Thus, we cannot use TestRestTemplate nor RestTemplate
 */
@WebMvcTest(MonkeyCatRobotController.class)
@TestPropertySource("classpath:application-test.yml")//
public class MonkeyCatRobotControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private MonkeyCatRobotService mCRService;

    private List<MonkeyCatRobot> expectedListMCRs;
    private List<MonkeyCatRobot> actualListMCRs;
    private MonkeyCatRobot mCR1;
    private MonkeyCatRobot mCR2;
    private MonkeyCatRobot mCR3;

    @BeforeEach
    void setUp() {
        //Given
        mCR1 =
                new MonkeyCatRobot(99L, "Engineer703", "123456789-1", "monkey", "orange", "triangle", "cat", "pink",
                        "oval", "robot", "purple");
        mCR2 =
                new MonkeyCatRobot(3021L, "Unit3021", "123456789-7", "robot", "pink", "trapezium", "monkey", "pink",
                        "rectangular", "cat", "grey");
        mCR3 =
                new MonkeyCatRobot(9042L, "Supervisor72", "123456789-7", "cat", "orange", "square", "robot", "magenta",
                        "circle", "monkey", "yellow");
        expectedListMCRs = new ArrayList<>();
        expectedListMCRs.add(mCR1);
        expectedListMCRs.add(mCR2);
        expectedListMCRs.add(mCR3);

        actualListMCRs = new ArrayList<>();
        actualListMCRs.add(mCR1);
        actualListMCRs.add(mCR2);
        actualListMCRs.add(mCR3);
    }

    @Test
    void getAll_ReturnsAllMCRs() throws Exception {
        //when
        when(mCRService.getAll()).thenReturn(actualListMCRs);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/monkeycatrobots/all")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String expectedResponseBody = objectMapper.writeValueAsString(expectedListMCRs);

        //then
        assertThat(actualResponseBody).isEqualTo(expectedResponseBody);
    }

    @Test
    void getById_ReturnsMCR() throws Exception {
        //when
        when(mCRService.getById(99L)).thenReturn(actualListMCRs.get(0));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/monkeycatrobots/id/99")
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        String actualResult = mvcResult.getResponse().getContentAsString();
        String expectedResult = objectMapper.writeValueAsString(mCR1);

        //then
        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    void post_Returns200() throws Exception {
        //given
        MonkeyCatRobot newMCR =
                new MonkeyCatRobot(499L, "Engineer499", "123456789-9", "robot", "lavender", "square", "monkey", "blue",
                        "circle", "cat", "azure");

        //when & then
        mockMvc.perform(MockMvcRequestBuilders.post("/monkeycatrobots/create")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(newMCR)))
                .andExpect(status().isOk());
    }

    @Test
    void postCreateMCR_MapsToModel() throws Exception {
        //given
        MonkeyCatRobot newMCR =
                new MonkeyCatRobot(499L, "Engineer499", "123456789-9", "robot", "lavender", "square", "monkey", "blue",
                        "circle", "cat", "azure");

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/monkeycatrobots/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(newMCR)));
        ArgumentCaptor<MonkeyCatRobot> mCRCaptor = ArgumentCaptor.forClass(MonkeyCatRobot.class);

        //then
        verify(mCRService, times(1)).createMonkeyCatRobot(mCRCaptor.capture());
        assertThat(mCRCaptor.getValue().getName()).isEqualTo("Engineer499");
        assertThat(mCRCaptor.getValue().getHeadType()).isEqualTo("robot");
    }

}

