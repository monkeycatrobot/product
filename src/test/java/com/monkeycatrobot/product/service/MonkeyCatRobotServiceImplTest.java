package com.monkeycatrobot.product.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.monkeycatrobot.product.model.MonkeyCatRobot;
import com.monkeycatrobot.product.repository.MonkeyCatRobotRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MonkeyCatRobotServiceImplTest {

    @Mock
    private MonkeyCatRobotRepository repository;

    @InjectMocks
    private MonkeyCatRobotServiceImpl service;

    private MonkeyCatRobot expectMCR1;
    private MonkeyCatRobot expectMCR2;
    private MonkeyCatRobot expectMCR3;
    private List<MonkeyCatRobot> mCRs;

    @BeforeEach
    void setUp() {
        //Given input
        mCRs = new ArrayList<>();
        expectMCR1 =
                new MonkeyCatRobot(99L, "Engineer703", "123456789-1", "monkey", "orange", "triangle", "cat", "pink",
                        "oval",
                        "robot", "purple");
        expectMCR2 =
                new MonkeyCatRobot(3021L, "Unit3021", "123456789-7", "robot", "pink", "trapezium", "monkey", "pink",
                        "rectangular", "cat", "grey");
        expectMCR3 =
                new MonkeyCatRobot(9042L, "Supervisor72", "123456789-7", "cat", "orange", "square", "robot", "magenta",
                        "circle", "monkey", "yellow");
        mCRs.add(expectMCR1);
        mCRs.add(expectMCR2);
        mCRs.add(expectMCR3);
    }

    @Test
    @DisplayName("return the count of all mCRs")
    void getCount() {
        //When action
        when(repository.count()).thenReturn(3L);
        Long count = repository.count();

        //Then output
        assertEquals(3, count, "should return a count of 3");
    }

    @Test
    @DisplayName("return all mCRs")
    void getAll() {
        //When
        when(repository.findAll()).thenReturn(mCRs);
        List<MonkeyCatRobot> mCRsAll = repository.findAll();

        //Then
        assertEquals(3, mCRsAll.size(), "should equal the size of list mCRs, which is 3");
    }

    @Test
    @DisplayName("return a monkeyCatRobot by id")
    void getById() {
        //when
        when(repository.findById(99L)).thenReturn(Optional.of(expectMCR1));
        Optional<MonkeyCatRobot> id99MCR = repository.findById(99L);

        //then
        Assertions.assertTrue(id99MCR.isPresent(), "should be present");
        assertEquals(Optional.of(expectMCR1), id99MCR, "should equal expectMCR1");
    }

    @Test
    @DisplayName("return a monkeyCatRobot by a mCR name")
    void getByName() {
        //when
        when(repository.findByNameIgnoreCase("Unit3021")).thenReturn(
                Collections.singletonList(mCRs.get(1)));
        List<MonkeyCatRobot> unit3021 = repository.findByNameIgnoreCase("Unit3021");

        //then
        assertEquals(Collections.singletonList(mCRs.get(1)), unit3021,
                "should equal the monkeyCatRobot at the second index of list mCRs");
    }


    @Test
    @DisplayName("return all mCRs associated with a customer's id")
    void getByCustomerId() {
        //given
        List<MonkeyCatRobot> mCRsWithSameCustomerId = new ArrayList<>();
        mCRsWithSameCustomerId.add(expectMCR2);
        mCRsWithSameCustomerId.add(expectMCR3);

        //when
        when(repository.findByCustomerId("123456789-7")).thenReturn(mCRsWithSameCustomerId);
        List<MonkeyCatRobot> customer123456789_7 = repository.findByCustomerId("123456789-7");

        //then
        assertEquals(mCRsWithSameCustomerId, customer123456789_7,
                "should equal list of every monkeyCatRobot with same customer id");
    }

    @Test
    @DisplayName("return a monkeyCatRobot by customer id & mCR name")
    void getByCustomerIdAndName() {
        //when
        when(repository.findByCustomerIdAndNameIgnoreCase("123456789-3021", "Unit3021")).thenReturn(
                Collections.singletonList(mCRs.get(1)));
        List<MonkeyCatRobot> unit3021 = repository.findByCustomerIdAndNameIgnoreCase("123456789-3021", "Unit3021");

        //then
        assertEquals(Collections.singletonList(mCRs.get(1)), unit3021,
                "should equal a list containing the monkeyCatRobot at the second index of list mCRs");
    }

    @Test
    @DisplayName("create a monkeyCatRobot")
    void createMonkeyCatRobot() {
        //given
        MonkeyCatRobot newMCR =
                new MonkeyCatRobot(101L, "Accountant101", "123456789-5", "cat", "purple", "circle", "robot", "green",
                        "rhomboid",
                        "monkey", "yellow");

        //when
        when(repository.save(newMCR)).thenAnswer(invocation -> {
            mCRs.add(newMCR);
            return null;
        });
        repository.save(newMCR);

        //then
        assertEquals(4, mCRs.size(), "should equal 4");
        assertEquals(101, mCRs.get(3).getId(), "should equal 101");
    }

    @Test
    @DisplayName("update a monkeyCatRobot")
    void updateMonkeyCatRobot() {
        //given
        MonkeyCatRobot mCRToUpdate = mCRs.get(0);
        MonkeyCatRobot detailsFromPutMCR =
                new MonkeyCatRobot(null, "Planner74", "123456789-5", "cat", "purple", "circle", "robot", "green",
                        "rhomboid", "monkey", "yellow");

        //when
        when(repository.save(mCRToUpdate)).thenAnswer(invocation -> {
            mCRToUpdate.setName(detailsFromPutMCR.getName());
            mCRToUpdate.setCustomerId(detailsFromPutMCR.getCustomerId());
            mCRToUpdate.setHeadType(detailsFromPutMCR.getHeadType());
            mCRToUpdate.setHeadColor(detailsFromPutMCR.getHeadColor());
            mCRToUpdate.setHeadShape(detailsFromPutMCR.getHeadShape());
            mCRToUpdate.setTorsoType(detailsFromPutMCR.getTorsoType());
            mCRToUpdate.setTorsoColor(detailsFromPutMCR.getTorsoColor());
            mCRToUpdate.setTorsoShape(detailsFromPutMCR.getTorsoShape());
            mCRToUpdate.setLegsType(detailsFromPutMCR.getLegsType());
            mCRToUpdate.setLegsColor(detailsFromPutMCR.getLegsColor());
            return null;
        });
        repository.save(mCRToUpdate);

        //then
        assertEquals("Planner74", mCRToUpdate.getName(), "should equal 'Planner74");
        assertEquals("123456789-5", mCRToUpdate.getCustomerId(), "should equal '123456789-5'");
        assertEquals("cat", mCRToUpdate.getHeadType(), "should equal 'cat'");
        assertEquals("purple", mCRToUpdate.getHeadColor(), "should equal 'purple'");
        assertEquals("circle", mCRToUpdate.getHeadShape(), "should equal 'circle'");
        assertEquals("robot", mCRToUpdate.getTorsoType(), "should equal 'robot'");
        assertEquals("green", mCRToUpdate.getTorsoColor(), "should equal 'green'");
        assertEquals("rhomboid", mCRToUpdate.getTorsoShape(), "should equal 'rhomboid'");
        assertEquals("monkey", mCRToUpdate.getLegsType(), "should equal 'monkey'");
        assertEquals("yellow", mCRToUpdate.getLegsColor(), "should equal 'yellow'");
    }

    @Test
    @DisplayName("delete a monkeyCatRobot")
    void deleteMonkeyCatRobot() {
        //given
        MonkeyCatRobot deleteThisMCR = mCRs.get(0);

        //when
        doAnswer(invocation -> {
            mCRs.remove(0);
            return null;
        }).when(repository).delete(deleteThisMCR);
        repository.delete(deleteThisMCR);

        //then
        assertEquals(2, mCRs.size());
        verify(repository).delete(deleteThisMCR);
    }
}